defmodule Scar.MixProject do
  use Mix.Project

  def project do
    [ app: :scar,
      version: "0.1.2",
      name: "scar",
      source_url: "https://gitlab.com/jonnystorm/scar",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      dialyzer: [
        add_plt_apps: [
          :logger,
          :gen_stage,
          :plug_cowboy,
          :netaddr_ex,
        ],
        ignore_warnings: "dialyzer.ignore",
        flags: [
          :unmatched_returns,
          :error_handling,
          :race_conditions,
          :underspecs,
        ],
      ],
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [ extra_applications: [
        :logger,
        :plug_cowboy,
        :eex,
      ],
    ]
    |> Keyword.merge(application(Mix.env))
  end

  def application(:test),
    do: []

  def application(:dev) do
    [ mod: {Scar, []},
      env: [
        collectors: [2055, 2056],
        http_port: 8000,
        log_level: "info",
      ],
    ]
  end

  def application(_),
    do: [mod: {Scar, []}]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [ {:distillery, "~> 2.1"},
      {:toml, "~> 0.3"},
      {:gen_stage, "~> 0.14"},
      {:plug_cowboy, "~> 2.0"},
      { :netaddr_ex,
        git: "https://gitlab.com/jonnystorm/netaddr-elixir.git"
      },
      {:poison, "~> 3.1"},
    ]
  end
end
