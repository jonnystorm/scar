defmodule Scar.UtilityTest do
  use ExUnit.Case

  import Scar.Utility

  @source URI.parse("tcp://73.51.197.98:60211")
  @napt   URI.parse("tcp://72.36.94.10:443")
  @target URI.parse("tcp://172.31.255.110:443")

  @test_graph %{
      nodes: %{
        {:foreign_source, @source.host} =>
          {:foreign_source, @source},

        "#{@napt}" =>
          {:translation, @napt},

        @target.host =>
          {:native, @target},
      },
      edges: %{
        {@source.host, "#{@target}"} =>
          %{src: {:foreign_source, @source},
            tgt: {:native, @target},
          },
        {"#{@napt}", "#{@target}"} =>
          %{src: {:translation, @napt},
            tgt: {:native, @target},
          },
      },
    }

  test "converts NetFlow to graph" do
    netflow =
      %{record: [
          nf_f_conn_id: 3498870,
          nf_f_src_addr_ipv4: <<73, 51, 197, 98>>,
          nf_f_src_port: 60211,
          nf_f_src_intf_id: 3,
          nf_f_dst_addr_ipv4: "H$^\n",
          nf_f_dst_port: 443,
          nf_f_dst_intf_id: 16,
          nf_f_protocol: 6,
          nf_f_icmp_type: 0,
          nf_f_icmp_code: 0,
          nf_f_xlate_src_addr_ipv4: <<73, 51, 197, 98>>,
          nf_f_xlate_dst_addr_ipv4: <<172, 31, 255, 110>>,
          nf_f_xlate_src_port: 60211,
          nf_f_xlate_dst_port: 443,
          nf_f_fw_event: 1,
          nf_f_fw_ext_event: 0,
          nf_f_event_time_msec: 1547524075267,
          nf_f_flow_create_time_msec: 1547524065248,
          nf_f_ingress_acl_id: %{
            ace_id: 254305194,
            acl_id: 102699556,
            extended_ace_id: 0
          },
          nf_f_egress_acl_id: %{
            ace_id: 0,
            acl_id: 0,
            extended_ace_id: 0
          },
          nf_f_username: nil
        ],
        sequence: 23,
        source: <<172, 31, 255, 1>>,
        source_id: 0,
        sys_uptime: 2000749114,
        unix_secs: 1547524080,
        version: 9
      }

    assert netflow_to_graph(netflow) == @test_graph
  end

  test "delete a node from a graph" do
    key = "tcp://72.36.94.10:443"

    graph =
      %{nodes: %{
          {:foreign_source, @source.host} =>
            {:foreign_source, @source},

          @target.host =>
            {:native, @target},
        },
        edges: %{
          {@source.host, "#{@target}"} =>
            %{src: {:foreign_source, @source},
              tgt: {:native, @target},
            },
        },
      }

    inc1 = incidences(@test_graph)
    inc2 = incidences(graph)

    expected = {inc2, graph}

    assert delete_node(key, inc1, @test_graph) == expected
  end
end
