# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

defmodule Scar.Grapher do
  use GenStage

  alias Scar.Utility

  require Logger

  def handle_events(events, _, state) do
    next_state =
      Enum.reduce(events, state, fn(netflow, acc) ->
        try do
          graph = Utility.netflow_to_graph(netflow)

          nodes =
            acc.nodes
            |> Map.merge(graph.nodes, fn _, v, _ -> v end)

          last_seen =
            with {:ok, now} <- DateTime.now("Etc/UTC"),
              do: DateTime.to_iso8601(now)

          edges =
            graph.edges
            |> Enum.reduce(acc.edges, fn({k, v1}, acc) ->
              next_v =
                if v2 = acc[k] do
                  v2
                else
                  v1
                end
                |> Map.update(:count, 1, & &1 + 1)
                |> Map.put(:last_seen, last_seen)

              Map.put(acc, k, next_v)
            end)

          %{acc|nodes: nodes, edges: edges}

        rescue
          e ->
            :ok = Logger.error("Failed to convert netflow to graph: #{inspect(netflow)}")

            reraise(e, __STACKTRACE__)
        end
      end)

    {:noreply, [], next_state}
  end

  def handle_call(:graph, _from, state),
    do: {:reply, state, [], state}

  def graph(
    pid_or_name,
    node_match \\ fn _ -> true end,
    opts \\ []
  )

  def graph(pid_or_name, node_match, opts)
      when is_function(node_match)
  do
    call_timeout0 = opts[:timeout]

    call_timeout =
      if  call_timeout0 != nil
          and is_integer(call_timeout0)
          and call_timeout0 > 0,
        do: call_timeout0,
      else: 5000

    graph =
      GenServer.call(pid_or_name, :graph, call_timeout)

    [_, unmatched0] =
      graph.nodes
      |> Enum.split_with(fn {_, {_, n}} ->
        node_match.("#{n.host}")
      end)
      |> Tuple.to_list
      |> Enum.map(&Enum.into(&1, %{}))

    unmatched = Map.keys(unmatched0)

    :ok = Logger.debug("Filtering out graph nodes #{inspect(unmatched)}")

    inc = Utility.incidences(graph)

    {_, next_graph} =
      Enum.reduce(unmatched, {inc, graph},
        fn(key, {i, g}) ->
          Utility.delete_node(key, i, g)
        end
      )

    next_graph
  end

  def init(args) do
    sources = Keyword.fetch!(args, :sources)

    state = %{nodes: %{}, edges: %{}}

    {:consumer, state, subscribe_to: sources}
  end

  def start_link(sources, opts \\ [])

  def start_link(sources, opts)
      when is_list(sources)
       and is_list(opts)
  do
    GenStage.start_link(
      __MODULE__,
      [sources: sources],
      [{:min_demand, 1}, {:max_demand, 1}|opts]
    )
  end

  def start_link(_, _),
    do: {:error, :einval}
end
