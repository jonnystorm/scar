defmodule Scar.Server do
  @moduledoc false

  require EEx
  require Logger

  use Plug.Router
  use NetAddr

  plug :match
  plug :dispatch
  plug Plug.Logger
  plug Plug.Parsers,
    parsers: [:urlencoded]

  alias Scar.{Grapher, Utility}

  defp reserved_regexes do
    ~p(0.0.0.0/8
       10.0.0.0/8
       100.64.0.0/10
       127.0.0.0/8
       169.254.0.0/16
       172.16.0.0/12
       192.0.0.0/24
       192.0.2.0/24
       192.88.99.0/24
       192.168.0.0/16
       198.18.0.0/15
       198.51.100.0/24
       203.0.113.0/24
       224.0.0.0/4
       240.0.0.0/4
       255.255.255.255/32
    )
    |> Enum.map(&NetAddr.netaddr_to_regex/1)
  end

  defp ip_strings_to_regexes(strings) do
    Enum.reduce(strings, [], fn (str, acc) ->
      case NetAddr.ip_2(str) do
        {:ok, netaddr} ->
          [NetAddr.netaddr_to_regex(netaddr)|acc]

        {:error, _} ->
          acc
      end
    end)
  end

  defp render_graph_template(template_path, graph) do
    nodes = Map.values(graph.nodes)
    edges = Map.values(graph.edges)

    :scar
    |> Application.app_dir(template_path)
    |> EEx.eval_file([nodes: nodes, edges: edges])
  end

  defp conn_to_graph(conn) do
    conn = Plug.Conn.fetch_query_params(conn)

    filter_regexes =
      conn.params
      |> Map.get("filter", "")
      |> String.split(",")
      |> ip_strings_to_regexes
      |> Enum.concat(reserved_regexes())

    delete_regexes =
      conn.params
      |> Map.get("delete", "")
      |> String.split(",")
      |> ip_strings_to_regexes

    node_match =
      fn node ->
        Enum.any?(filter_regexes, & node =~ &1)
          and not Enum.any?(delete_regexes, & node =~ &1)
      end

    Grapher.graph(Scar.Grapher, node_match)
  end


  ######################### Routes #########################

  get "/graph.svg" do
    g = conn_to_graph(conn)

    dot =
      "priv/templates/graph.dot.eex"
      |> render_graph_template(g)

    svg = Utility.execute_dot(:svg, dot)

    conn
    |> put_resp_content_type("image/svg+xml")
    |> resp(:ok, svg)
  end

  get "/graph.dot" do
    g = conn_to_graph(conn)

    dot =
      "priv/templates/graph.dot.eex"
      |> render_graph_template(g)

    conn
    |> put_resp_content_type("text/vnd.dot")
    |> resp(:ok, dot)
  end

  get "/graph.graphml" do
    g = conn_to_graph(conn)

    graphml =
      "priv/templates/graph.graphml.eex"
      |> render_graph_template(g)

    conn
    |> put_resp_content_type("application/graphml+xml")
    |> resp(:ok, graphml)
  end

  get "/flows.json" do
    conn = Plug.Conn.fetch_query_params(conn)

    g = conn_to_graph(conn)

    json =
      g.edges
      |> Enum.map(fn {k, v} ->
        {src, dst} = k

        v
        |> Map.take([:count, :last_seen])
        |> Map.put(:source, src)
        |> Map.put(:destination, dst)
      end)
      |> Enum.sort_by(fn %{count: c} -> c end, &>=/2)
      |> Poison.encode!

    conn
    |> put_resp_content_type("application/json")
    |> resp(:ok, json)
  end

  match _,
    do: resp(conn, :not_found, "")

end
