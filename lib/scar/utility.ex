# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

defmodule Scar.Utility do
  @moduledoc false

  require Logger

  defp node_to_key({type, uri}) do
    case type do
      :native ->
        uri.host

      :translation ->
        "#{uri}"

      :foreign_source ->
        # Making this node unique by host and flow direction
        # avoids storing a node for every dynamic port.
        # However, we preserve the ability to make a new
        # node for the same host in the other direction, as
        # a destination.
        #
        {type, uri.host}
    end
  end

  def edge_to_node_keys(%{src: t, tgt: h}),
    do: Enum.map([t, h], &node_to_key/1)

  def edge_to_edge_key(
    %{src: {:translation, t}, tgt: {_, h}}
  ),
    do: {"#{t}", "#{h}"}

  def edge_to_edge_key(%{src: {_, t}, tgt: {_, h}}),
    do: {t.host, "#{h}"}

  defp _delete_node(key, incidences, graph) do
    node_incidences = incidences[key]

    neighbor_keys =
      node_incidences
      |> Enum.flat_map(&edge_to_node_keys/1)
      |> Enum.filter(& &1 != key)
      |> Enum.uniq

    {node_keys_to_drop, edges_to_drop} =
      incidences
      |> Map.take([key|neighbor_keys])
      |> Enum.map(fn {k, v} ->
        {k, Enum.split_with(v, & &1 in node_incidences)}
      end)
      |> Enum.reduce({[], []}, fn
        ({k, {drop,    []}}, {nodes, edges}) ->
          next_edges =
            drop
            |> Enum.concat(edges)
            |> Enum.uniq

          {[k|nodes], next_edges}

        ({_, {drop, _keep}}, {nodes, edges}) ->
          next_edges =
            drop
            |> Enum.concat(edges)
            |> Enum.uniq

          {nodes, next_edges}
      end)

    edge_keys_to_drop =
      Enum.map(edges_to_drop, &edge_to_edge_key/1)

    :ok = Logger.debug("Omitting the following graph nodes: #{inspect(node_keys_to_drop)}")
    :ok = Logger.debug("Omitting the following graph edges: #{inspect(edge_keys_to_drop)}")

    next_incidences =
      incidences
      |> Enum.filter(fn {k, _} ->
        k not in node_keys_to_drop
      end)
      |> Enum.map(fn {k, v} ->
        {k, Enum.filter(v, & &1 not in edges_to_drop)}
      end)
      |> Enum.into(%{})

    {_, next_nodes} =
      Map.split(graph.nodes, node_keys_to_drop)

    {_, next_edges} =
      Map.split(graph.edges, edge_keys_to_drop)

    { next_incidences,
      %{graph |
        nodes: next_nodes,
        edges: next_edges,
      }
    }
  end

  def delete_node(key, graph) do
    incidences = incidences(graph)

    {_, new_graph} =
      delete_node(key, incidences, graph)

    new_graph
  end

  # This interface is provided as an optimization for
  # repeated calls. In this way, we avoid recalculating
  # incidences for each call when reducing over a list of
  # keys to delete. The other interface is left for
  # convenience.
  #
  def delete_node(key, incidences, graph) do
    if incidences[key] do
      _delete_node(key, incidences, graph)
    else
      {incidences, graph}
    end
  end

  defp _incidences([], acc),
    do: acc

  defp _incidences([e|rest], acc) do
    next_acc =
      e
      |> edge_to_node_keys
      |> Enum.reduce(acc, fn (k, acc1) ->
        Map.update(acc1, k, [e], & [e|&1])
      end)

    _incidences(rest, next_acc)
  end

  def incidences(graph) do
    graph.edges
    |> Map.values
    |> _incidences(%{})
  end

  def netflow_to_graph(netflow) do
    # Depending on whether the flow indicates a translation
    # has occurred, we apply one of three computational
    # contexts, or "types," to each node. A flow with a
    # translation produces all three node types with two
    # edges between them
    #
    #     foreign_source -> translation -> native
    #
    # where `foreign_source` is the initiator, which knows
    # only the mapped destination, `translation` is the
    # mapped destination, and `native` is the real
    # destination.
    #
    # A flow without a translation simply produces two
    # `native` nodes, connected by a single edge:
    #
    #     native -> native.
    #
    # In essence, every edge is a flow, but an edge from a
    # translation node to a native node represents a class
    # of flows from every possible foreign source to the
    # mapped native node.
    #
    ip_proto_to_str =
      fn num ->
        %{ 51 => "ah",
           88 => "eigrp",
           50 => "esp",
           47 => "gre",
            1 => "icmp",
           58 => "icmp6",
            2 => "igmp",
            9 => "igrp",
            0 => "ip",
            4 => "ipinip",
           94 => "nos",
           89 => "ospf",
          108 => "pcp",
          103 => "pim",
           45 => "pptp",
          109 => "snp",
            6 => "tcp",
           17 => "udp",
        }[num]
      end

    ip_bin_to_str =
      fn bin ->
        bin
        |> :binary.bin_to_list
        |> Enum.join(".")
      end

    flow =
      %{proto: netflow.record[:nf_f_protocol],
        src:   netflow.record[:nf_f_src_addr_ipv4],
        dst:   netflow.record[:nf_f_dst_addr_ipv4],
        spt:   netflow.record[:nf_f_src_port],
        dpt:   netflow.record[:nf_f_dst_port],
        snat:  netflow.record[:nf_f_xlate_src_addr_ipv4],
        dnat:  netflow.record[:nf_f_xlate_dst_addr_ipv4],
        spat:  netflow.record[:nf_f_xlate_src_port],
        dpat:  netflow.record[:nf_f_xlate_dst_port],
        exporter: netflow.source,
      }
      |> Map.update(:proto, nil, ip_proto_to_str)
      |> Map.update(:src,   nil, ip_bin_to_str)
      |> Map.update(:dst,   nil, ip_bin_to_str)
      |> Map.update(:snat,  nil, ip_bin_to_str)
      |> Map.update(:dnat,  nil, ip_bin_to_str)
      |> Map.update(:exporter, nil, ip_bin_to_str)

    if flow.dst != flow.dnat
       or flow.dpt != flow.dpat
    do
      source =
        { :foreign_source,
          "#{flow.proto}://#{flow.src}:#{flow.spt}"
          |> URI.parse
        }

      napt =
        { :translation,
          "#{flow.proto}://#{flow.dst}:#{flow.dpt}"
          |> URI.parse
        }

      target =
        { :native,
          "#{flow.proto}://#{flow.dnat}:#{flow.dpat}"
          |> URI.parse
        }

      nodes =
        [source, napt, target]
        |> Enum.map(& {node_to_key(&1), &1})
        |> Enum.into(%{})

      edges =
        [ %{src: source, tgt: target},
          %{src: napt,   tgt: target},
        ]
        |> Enum.map(& {edge_to_edge_key(&1), &1})
        |> Enum.into(%{})

      %{nodes: nodes, edges: edges}
    else
      source =
        { :native,
          "#{flow.proto}://#{flow.src}:#{flow.spt}"
          |> URI.parse
        }

      target =
        { :native,
          "#{flow.proto}://#{flow.dst}:#{flow.dpt}"
          |> URI.parse
        }

      nodes =
        [source, target]
        |> Enum.map(& {node_to_key(&1), &1})
        |> Enum.into(%{})

      edges =
        [ %{src: source, tgt: target},
        ]
        |> Enum.map(& {edge_to_edge_key(&1), &1})
        |> Enum.into(%{})

      %{nodes: nodes, edges: edges}
    end
  end

  def stream_udp(port) when port in 1..65535 do
    opts =
      [ mode:   :binary,
        active: false,
        recbuf: 128000,
      ]

    Stream.resource(
      fn ->
        {:ok, sock} = :gen_udp.open(port, opts)

        sock
      end,
      fn sock ->
        case :gen_udp.recv(sock, 0) do
          {:ok, recvdata}  ->
            {[recvdata], sock}

          {:error, reason} ->
            :ok = Logger.error("Socket receive failed: #{inspect(reason)}")

            {:halt, sock}
        end
      end,
      fn sock -> :gen_udp.close(sock) end
    )
  end

  def execute_dot(format, dot)
      when format in [:png, :svg, :pdf]
  do
    safe = String.replace(dot, "'", "\"")
    int  = System.unique_integer([:positive])
    temp = Path.join(System.tmp_dir!(), "#{int}")
    args = "-T#{format} -o #{temp}"

    _ = File.touch!(temp)
    _ =
      """
      which dot &>/dev/null &&
        echo '#{safe}' |
        /usr/bin/env dot #{args} 2>/dev/null
      """
      |> String.to_charlist
      |> :os.cmd

    File.read!(temp)
  end
end
