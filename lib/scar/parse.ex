# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

defmodule Scar.Parse do
  require Logger

  defp format_acl_id(
    <<acl_id::32,
      ace_id::32,
      ext_ace_id::32
    >>
  ) do
    # From https://www.cisco.com/c/en/us/td/docs/security/asa/special/netflow/guide/asa_netflow.html#pgfId-1331296:
    #
    # All ACL IDs are composed of the following three
    # four-byte values:
    #
    # * Hash value or ID of the ACL name
    # * Hash value, ID, or line of an ACE within the ACL
    # * Hash value or ID of an extended ACE configuration
    #
    %{acl_id: acl_id,
      ace_id: ace_id,
      extended_ace_id: ext_ace_id,
    }
  end

  defp format_natural(data) do
    s = byte_size(data)

    <<value::size(s)-unit(8)>> = data

    value
  end

  defp _format_string(<<>>, <<>>),
    do: nil

  defp _format_string(acc, <<>>),
    do: acc

  defp _format_string(acc, <<0, rest::bits>>),
    do: _format_string(acc, rest)

  defp _format_string(acc, <<c, rest::bits>>),
    do: _format_string(acc <> <<c>>, rest)

  defp format_string(data),
    do: _format_string(<<>>, data)

  def netflow_types do
    # Some of these types come from RFC3954. Most come from
    # https://www.cisco.com/c/en/us/td/docs/security/asa/special/netflow/guide/asa_netflow.html#pgfId-1331296
    #
    %{1     => {4,  :in_bytes, &format_natural/1},
      2     => {4,  :in_pkts, &format_natural/1},
      3     => {4,  :flows, &format_natural/1},
      4     => {1,  :nf_f_protocol, &format_natural/1},
      5     => {1,  :tos},
      6     => {1,  :tcp_flags},
      7     => {2,  :nf_f_src_port, &format_natural/1},
      8     => {4,  :nf_f_src_addr_ipv4},
      9     => {1,  :src_mask},
      10    => {2,  :nf_f_src_intf_id, &format_natural/1},
      11    => {2,  :nf_f_dst_port, &format_natural/1},
      12    => {4,  :nf_f_dst_addr_ipv4},
      13    => {1,  :dst_mask},
      14    => {2,  :nf_f_dst_intf_id, &format_natural/1},
      15    => {4,  :ipv4_next_hop},
      16    => {2,  :src_as, &format_natural/1},
      17    => {2,  :dst_as, &format_natural/1},
      18    => {4,  :bgp_ipv4_next_hop},
      19    => {4,  :mul_dst_pkts, &format_natural/1},
      20    => {4,  :mul_dst_bytes, &format_natural/1},
      21    => {4,  :last_switched, &format_natural/1},
      22    => {4,  :first_switched, &format_natural/1},
      23    => {4,  :out_bytes, &format_natural/1},
      24    => {4,  :out_pkts, &format_natural/1},
      27    => {16, :nf_f_src_addr_ipv6},
      28    => {16, :nf_f_dst_addr_ipv6},
      29    => {1,  :ipv6_src_mask},
      30    => {1,  :ipv6_dst_mask},
      31    => {3,  :ipv6_flow_label},
      32    => {2,  :icmp_type, &format_natural/1},
      33    => {1,  :mul_igmp_type, &format_natural/1},
      34    => {4,  :sampling_interval, &format_natural/1},
      35    => {1,  :sampling_algorithm, &format_natural/1},
      36    => {2,  :flow_active_timeout, &format_natural/1},
      37    => {2,  :flow_inactive_timeout, &format_natural/1},
      38    => {1,  :engine_type, &format_natural/1},
      39    => {1,  :engine_id, &format_natural/1},
      40    => {4,  :total_bytes_exp, &format_natural/1},
      41    => {4,  :total_pkts_exp, &format_natural/1},
      42    => {4,  :total_flows_exp, &format_natural/1},
      46    => {1,  :mpls_top_label_type, &format_natural/1},
      47    => {4,  :mpls_top_label_ip_addr},
      48    => {1,  :flow_sampler_id, &format_natural/1},
      49    => {1,  :flow_sampler_mode, &format_natural/1},
      50    => {4,  :flow_sampler_random_interval, &format_natural/1},
      55    => {1,  :dst_tos},
      56    => {6,  :src_mac},
      57    => {6,  :dst_mac},
      58    => {2,  :src_vlan, &format_natural/1},
      59    => {2,  :dst_vlan, &format_natural/1},
      60    => {1,  :ip_protocol_version, &format_natural/1},
      61    => {1,  :direction, &format_natural/1},
      62    => {16, :ipv6_next_hop},
      63    => {16, :bgp_ipv6_next_hop},
      64    => {4,  :ipv6_option_headers},
      70    => {3,  :mpls_label_1, &format_natural/1},
      71    => {3,  :mpls_label_2, &format_natural/1},
      72    => {3,  :mpls_label_3, &format_natural/1},
      73    => {3,  :mpls_label_4, &format_natural/1},
      74    => {3,  :mpls_label_5, &format_natural/1},
      75    => {3,  :mpls_label_6, &format_natural/1},
      76    => {3,  :mpls_label_7, &format_natural/1},
      77    => {3,  :mpls_label_8, &format_natural/1},
      78    => {3,  :mpls_label_9, &format_natural/1},
      79    => {3,  :mpls_label_10, &format_natural/1},
      148   => {4,  :nf_f_conn_id, &format_natural/1},
      152   => {8,  :nf_f_flow_create_time_msec, &format_natural/1},
      176   => {1,  :nf_f_icmp_type, &format_natural/1},
      177   => {1,  :nf_f_icmp_code, &format_natural/1},
      178   => {1,  :nf_f_icmp_type_ipv6, &format_natural/1},
      179   => {1,  :nf_f_icmp_code_ipv6, &format_natural/1},
      225   => {4,  :nf_f_xlate_src_addr_ipv4},
      226   => {4,  :nf_f_xlate_dst_addr_ipv4},
      227   => {2,  :nf_f_xlate_src_port, &format_natural/1},
      228   => {2,  :nf_f_xlate_dst_port, &format_natural/1},
      231   => {4,  :nf_f_fwd_flow_delta_bytes, &format_natural/1},
      232   => {4,  :nf_f_rev_flow_delta_bytes, &format_natural/1},
      233   => {1,  :nf_f_fw_event, &format_natural/1},
      281   => {16, :nf_f_xlate_src_addr_ipv6},
      282   => {16, :nf_f_xlate_dst_addr_ipv6},
      323   => {8,  :nf_f_event_time_msec, &format_natural/1},
      33000 => {12, :nf_f_ingress_acl_id, &format_acl_id/1},
      33001 => {12, :nf_f_egress_acl_id, &format_acl_id/1},
      33002 => {2,  :nf_f_fw_ext_event, &format_natural/1},
      40000 => {20, :nf_f_username, &format_string/1},
      #40000 => {65, :nf_f_username_max},
    }
  end

  #    +--------+--------------------------------------------------------+
  #    |        | +----------+ +---------+     +-----------+ +---------+ |
  #    | Packet | | Template | | Data    |     | Options   | | Data    | |
  #    | Header | | FlowSet  | | FlowSet | ... | Template  | | FlowSet | |
  #    |        | |          | |         |     | FlowSet   | |         | |
  #    |        | +----------+ +---------+     +-----------+ +---------+ |
  #    +--------+--------------------------------------------------------+
  #
  #     0                   1                   2                   3
  #     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |          FlowSet ID           |           Length              |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |                              ...                              |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #
  defp _decode_netflow_v9_flowsets(<<>>, acc),
    do: Enum.reverse(acc)

  defp _decode_netflow_v9_flowsets(
    <<flowset_id::16,
      length::16,
      rest0::bits
    >>,
    acc
  ) do
    data_length = length - 4

    <<data::bytes-size(data_length),
      rest::bits
    >> = rest0

    _ = Logger.debug("Got flowset ID #{flowset_id} of #{length} bytes")

    next_acc = [{flowset_id, data}|acc]

    _decode_netflow_v9_flowsets(rest, next_acc)
  end

  def decode_netflow_v9_flowsets(data),
     do: _decode_netflow_v9_flowsets(data, [])

  defp decode_netflow_v9_template_fields(<<>>, acc),
    do: Enum.reverse(acc)

  defp decode_netflow_v9_template_fields(<<0, 0>>, acc),
    do: Enum.reverse(acc)

  defp decode_netflow_v9_template_fields(
    <<type::16,
      length::16,
      rest::bits
    >>,
    acc
  ) do
    field = %{type: type, length: length}

    decode_netflow_v9_template_fields(rest, [field|acc])
  end

  # 5.2.  Template FlowSet Format
  #
  #    One of the essential elements in the NetFlow format is the Template
  #    FlowSet.  Templates greatly enhance the flexibility of the Flow
  #    Record format because they allow the NetFlow Collector to process
  #    Flow Records without necessarily knowing the interpretation of all
  #    the data in the Flow Record.  The format of the Template FlowSet is
  #    as follows:
  #
  #     0                   1                   2                   3
  #     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |       FlowSet ID = 0          |          Length               |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |      Template ID 256          |         Field Count           |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |        Field Type 1           |         Field Length 1        |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |        Field Type 2           |         Field Length 2        |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |             ...               |              ...              |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |        Field Type N           |         Field Length N        |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |      Template ID 257          |         Field Count           |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |        Field Type 1           |         Field Length 1        |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |        Field Type 2           |         Field Length 2        |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |             ...               |              ...              |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |        Field Type M           |         Field Length M        |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |             ...               |              ...              |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |        Template ID K          |         Field Count           |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |             ...               |              ...              |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #
  #
  # 6.1.  Options Template FlowSet Format
  #
  #    The Options Template Record (and its corresponding Options Data
  #    Record) is used to supply information about the NetFlow process
  #    configuration or NetFlow process specific data, rather than supplying
  #    information about IP Flows.
  #
  #    For example, the Options Template FlowSet can report the sample rate
  #    of a specific interface, if sampling is supported, along with the
  #    sampling method used.
  #
  #    The format of the Options Template FlowSet follows.
  #
  #     0                   1                   2                   3
  #     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |       FlowSet ID = 1          |          Length               |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |         Template ID           |      Option Scope Length      |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |        Option Length          |       Scope 1 Field Type      |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |     Scope 1 Field Length      |               ...             |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |     Scope N Field Length      |      Option 1 Field Type      |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |     Option 1 Field Length     |             ...               |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |     Option M Field Length     |           Padding             |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #
  defp _decode_netflow_v9_templates(<<>>, _, acc),
    do: Enum.reverse(acc)

  defp _decode_netflow_v9_templates(
    <<id::16,
      count::16,
      fields0::size(count)-unit(32)-binary,
      rest::bits
    >>,
    0,
    acc
  )   when id > 255
  do
    fields =
      decode_netflow_v9_template_fields(fields0, [])

    next_acc = [{id, fields}|acc]

    _decode_netflow_v9_templates(rest, 0, next_acc)
  end

  defp _decode_netflow_v9_templates(
    <<id::16,
      scope_length::16,
      option_length::16,
      scopes0::bytes-size(scope_length),
      options0::bytes-size(option_length),
      rest::bits
    >>,
    1,
    acc
  )   when id > 255
  do
    # TODO: Scope length probably means "length of an
    #       individual scope definition in this template,"
    #       in which case, refactor. However, option length,
    #       I think, is correct. Dumb.
    #
    scopes =
      decode_netflow_v9_template_fields(scopes0, [])

    options =
      decode_netflow_v9_template_fields(options0, [])

    template =
      {id, %{scopes: scopes, options: options}}

    _decode_netflow_v9_templates(rest, 1, [template|acc])
  end

  def decode_netflow_v9_templates({id, data}),
    do: _decode_netflow_v9_templates(data, id, [])

  # 5.3.  Data FlowSet Format
  #
  #    The format of the Data FlowSet is as follows:
  #
  #     0                   1                   2                   3
  #     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |   FlowSet ID = Template ID    |          Length               |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |   Record 1 - Field Value 1    |   Record 1 - Field Value 2    |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |   Record 1 - Field Value 3    |             ...               |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |   Record 2 - Field Value 1    |   Record 2 - Field Value 2    |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |   Record 2 - Field Value 3    |             ...               |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |   Record 3 - Field Value 1    |             ...               |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |              ...              |            Padding            |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #
  #
  # 6.2.  Options Data Record Format
  #
  #    The Options Data Records are sent in Data FlowSets, on a regular
  #    basis, but not with every Flow Data Record.  How frequently these
  #    Options Data Records are exported is configurable.  See the
  #    "Templates Management" section for more details.
  #
  #    The format of the Data FlowSet containing Options Data Records
  #    follows.
  #
  #     0                   1                   2                   3
  #     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |    FlowSet ID = Template ID   |          Length               |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |   Record 1 - Scope 1 Value    |Record 1 - Option Field 1 Value|
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |Record 1 - Option Field 2 Value|             ...               |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |   Record 2 - Scope 1 Value    |Record 2 - Option Field 1 Value|
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |Record 2 - Option Field 2 Value|             ...               |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |   Record 3 - Scope 1 Value    |Record 3 - Option Field 1 Value|
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |Record 3 - Option Field 2 Value|             ...               |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |              ...              |            Padding            |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #
  defp _decode_netflow_v9(<<>>, _, state) do
    case Enum.reverse(state.record) do
      [] ->
        Enum.reverse(state.records)

      record ->
        Enum.reverse([record|state.records])
    end
  end

  defp _decode_netflow_v9(<<0, 0>>, template, state),
    do: _decode_netflow_v9(<<>>, template, state)

  defp _decode_netflow_v9(
    data,
    [],
    state
  ) do
    record = Enum.reverse(state.record)

    next_state =
      %{state |
        record:  [],
        records: [record|state.records],
      }

    _decode_netflow_v9(data, state.template, next_state)
  end

  defp _decode_netflow_v9(
    _data,
    %{scopes: _, options: _},
    state
  ) do
    # TODO: Actually decode options data records. For now,
    # just return existing records.
    #
    state.records
  end

  defp _decode_netflow_v9(
    data,
    [%{type: type, length: length}|tail],
    state
  ) do
    try do
      <<value::bytes-size(length),
        rest::bits
      >> = data

      next_record =
        case netflow_types()[type] do
          {_len, type_atom} ->
            [{type_atom, value}|state.record]

          {_len, type_atom, fun} ->
            [{type_atom, fun.(value)}|state.record]
        end

      next_state = %{state|record: next_record}

      _decode_netflow_v9(rest, tail, next_state)

    rescue
      e ->
        _ = Logger.error("Failed to parse #{inspect(data)} using field template #{inspect({type, length})}")

        reraise(e, __STACKTRACE__)
    end
  end

  def decode_netflow_v9(flowset, template) do
    _ = Logger.debug("Decoding NetFlow data record #{inspect(flowset)}")

    state =
      %{template: template,
        record:   [],
        records:  [],
      }

    _decode_netflow_v9(flowset, template, state)
  end

  # 5.1.  Header Format
  #
  #    The Packet Header format is specified as:
  #
  #     0                   1                   2                   3
  #     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |       Version Number          |            Count              |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |                           sysUpTime                           |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |                           UNIX Secs                           |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |                       Sequence Number                         |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  #    |                        Source ID                              |
  #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  def decode_header(
    <<version::16,
      count::16,
      sys_uptime::32,
      unix_secs::32,
      sequence::32,
      source_id::32,
      rest::bits
    >>
  ) do
    { %{version:    version,
        count:      count,
        sys_uptime: sys_uptime,
        unix_secs:  unix_secs,
        sequence:   sequence,
        source_id:  source_id,
      },
      rest
    }
  end

  def decode_header(_),
    do: raise "Unknown NetFlow header format"

  defp ungroup(map) do
    Enum.flat_map(map, fn {k, v} ->
      Enum.map(v, &{k, &1})
    end)
  end

  defp inspect_bin(bin) do
    bin
    |> :binary.bin_to_list
    |> Enum.join(", ")
    |> String.replace_prefix("", "<<")
    |> String.replace_suffix("", ">>")
  end

  # From [RFC3954](https://www.ietf.org/rfc/rfc3954.txt):
  #
  # 9.  The Collector Side
  #
  #    The Collector receives Template Records from the
  #    Exporter, normally before receiving Flow Data Records
  #    (or Options Data Records).  The Flow Data Records (or
  #    Options Data Records) can then be decoded and stored
  #    locally on the devices.  If the Template Records have
  #    not been received at the time Flow Data Records (or
  #    Options Data Records) are received, the Collector
  #    SHOULD store the Flow Data Records (or Options Data
  #    Records) and decode them after the Template Records
  #    are received.  A Collector device MUST NOT assume
  #    that the Data FlowSet and the associated Template
  #    FlowSet (or Options Template FlowSet) are exported in
  #    the same Export Packet.
  #
  #    The Collector MUST NOT assume that one and only one
  #    Template FlowSet is present in an Export Packet.
  #
  #    The life of a template at the Collector is limited to
  #    a fixed refresh timeout.  Templates not refreshed
  #    from the Exporter within the timeout are expired at
  #    the Collector.  The Collector MUST NOT attempt to
  #    decode the Flow or Options Data Records with an
  #    expired Template.  At any given time the Collector
  #    SHOULD maintain the following for all the current
  #    Template Records and Options Template Records:
  #    Exporter, Observation Domain, Template ID, Template
  #    Definition, Last Received.
  #
  #    Note that the Observation Domain is identified by the
  #    Source ID field from the Export Packet.
  #
  #    In the event of a clock configuration change on the
  #    Exporter, the Collector SHOULD discard all Template
  #    Records and Options Template Records associated with
  #    that Exporter, in order for Collector to learn the
  #    new set of fields: Exporter, Observation Domain,
  #    Template ID, Template Definition, Last Received.
  #
  #    Template IDs are unique per Exporter and per
  #    Observation Domain.
  #
  #    If the Collector receives a new Template Record (for
  #    example, in the case of an Exporter restart) it MUST
  #    immediately override the existing Template Record.
  #
  #    Finally, note that the Collector MUST accept padding
  #    in the Data FlowSet and Options Template FlowSet,
  #    which means for the Flow Data Records, the Options
  #    Data Records and the Template Records. Refer to the
  #    terminology summary table in Section 2.1.
  #
  def reduce_netflow({ip, _, data}, acc) do
    # TODO: Per RFC 3954, the collector is responsible for
    #       template expiry, but how? And when?
    #
    {header, rest} = decode_header(data)

    src = {ip, header.source_id}

    next_seq  = Map.get(acc.next_seq, src, [])
    templates = Map.get(acc.templates, src, %{})
    unparsed0 = Map.get(acc.unparsed, src, %{})

    {template_flowsets, unparsed} =
      rest
      |> decode_netflow_v9_flowsets
      |> Enum.reduce(unparsed0, fn({k, v}, acc) ->
        _ = Logger.debug("Got flowset ID #{k}: #{inspect_bin(v)}")

        Map.update(acc, k, [v], & [v|&1])
      end)
      |> Map.split([0, 1])

    next_templates =
      template_flowsets
      |> ungroup
      |> Enum.flat_map(&decode_netflow_v9_templates/1)
      |> Enum.reduce(templates, fn({k, v}, acc) ->
        Map.put(acc, k, v)
      end)

    _ = Logger.debug("Using templates #{inspect(Map.keys(next_templates))}")

    {parseable, unparseable} =
      Map.split(unparsed, Map.keys(next_templates))

    _ =
      if unparseable != %{} do
        ids =
          unparseable
          |> Map.keys
          |> Enum.join(", ")

        Logger.warn("Missing templates for the following flowset IDs: #{ids}")
      end

    records =
      parseable
      |> ungroup
      |> Enum.flat_map(fn {i, d} ->
        _ = Logger.debug("Decoding #{inspect_bin(d)} using template #{i}")

        decode_netflow_v9(d, next_templates[i])
      end)
      |> Enum.map(fn fields ->
        header
        |> Map.drop([:count])
        |> Map.put(:source, ip)
        |> Map.put(:record, fields)
      end)

    next_next_sequence =
      next_seq
      |> List.delete(header.sequence)
      |> List.insert_at(0, header.sequence + 1)

    next_acc =
      %{acc |
        :next_seq  =>
          Map.put(acc.next_seq, src, next_next_sequence),

        :templates =>
          Map.put(acc.templates, src, next_templates),

        :unparsed  =>
          Map.put(acc.unparsed, src, unparseable),
      }

    if records == [] do
      {:cont, next_acc}
    else
      {:cont, records, next_acc}
    end
  end
end
