# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

defmodule Scar.Collector do
  use GenStage

  alias Scar.Parse

  def handle_events(events, _, state) do
    {records0, next_state} =
      Enum.reduce(events, {[], state},
        fn (e, {acc, state0}) ->
          case Parse.reduce_netflow(e, state0) do
            {:cont, state1} ->
              {acc, state1}

            {:cont, records, state1} ->
              {Enum.concat(records, acc), state1}
          end
      end)

    records = Enum.reverse(records0)

    {:noreply, records, next_state}
  end

  def init(args) do
    source = Keyword.fetch!(args, :source)
    state  =
      %{next_seq:  %{},
        templates: %{},
        unparsed:  %{},
      }

    {:producer_consumer, state, subscribe_to: [source]}
  end

  def start_link(source, opts \\ []) do
    GenStage.start_link(__MODULE__, [source: source], opts)
  end
end
