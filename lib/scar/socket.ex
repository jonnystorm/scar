# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

defmodule Scar.Socket do
  use GenStage

  def handle_info({:udp, _, ip0, port, data}, state) do
    ip = 
      ip0
      |> Tuple.to_list
      |> :binary.list_to_bin

    {:noreply, [{ip, port, data}], state}
  end

  def handle_demand(_demand, state),
    do: {:noreply, [], state}

  def init(args) do
    uri  = args[:uri]
    sock =
      case uri.scheme do
        "udp" ->
          opts =
            [ mode: :binary,
              active: true,
              recbuf: 128000,
            ]

          {:ok, sock} = :gen_udp.open(uri.port, opts)

          sock
      end

    state = %{uri: uri, socket: sock}

    {:producer, state}
  end

  def start_link(
    %URI{scheme: scheme, port: port} = uri,
    opts \\ []
  )   when scheme in ["udp"]
       and port in 1..65535
  do
    GenStage.start_link(__MODULE__, [uri: uri], opts)
  end
end
