# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

defmodule Scar do
  use Application

  alias Scar.{Socket, Collector, Grapher}

  defp collection_specs(port, opts0 \\ []) do
    uri = URI.parse("udp://localhost:#{port}")

    socket_id    = :"Scar.Socket(#{uri}"
    collector_id = :"Scar.Collector(#{uri})"

    opts = Keyword.take(opts0, [:restart])

    { collector_id,
      [ %{id: socket_id,
          start: {
            Socket,
            :start_link,
            [uri, [{:name, socket_id}|opts]]
          },
        },
        %{id: collector_id,
          start: {
            Collector,
            :start_link,
            [socket_id, [{:name, collector_id}|opts]]
          },
        },
      ]
    }
  end

  def start(_type, _args) do
    log_level =
      :scar
      |> Application.get_env(:log_level, "info")
      |> String.to_existing_atom

    :ok = Logger.configure(level: log_level)

    ports =
      :scar
      |> Application.get_env(:collectors, [])
      |> Enum.filter(& &1 in 1..65535)

    {collectors, specs0} =
      ports
      |> Enum.map(&collection_specs/1)
      |> Enum.unzip

    collector_specs = List.flatten(specs0)

    grapher_spec =
      %{id: Scar.Grapher,
        start: {
          Grapher,
          :start_link,
          [collectors, [name: Scar.Grapher]]
        },
      }

    port =
      Application.get_env(:scar, :http_port, 8000)

    server_spec =
      [ plug: Scar.Server,
        scheme: :http,
        options: [port: port],
      ]
      |> Plug.Adapters.Cowboy.child_spec

    specs =
      collector_specs
      |> Enum.concat([grapher_spec, server_spec])

    params =
      [ name: Scar.Supervisor,
        strategy: :one_for_one,
      ]

    Supervisor.start_link(specs, params)
  end
end
