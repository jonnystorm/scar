# Scar

Map service dependencies using NetFlows

## Usage

First, build a release and extract it:

```
mix do deps.get, deps.compile
MIX_ENV=prod mix distillery.release
cp _build/prod/rel/scar/releases/$version/scar.tar.gz ~
cd
tar xzf scar.tar.gz --one-top-level=scar
cd scar
```

Modify `config.toml`:

```toml
# NetflowV9 w/ UDP, only
#
[scar]
collectors = [2055, 2058..2059]
http_port  = 8000
```

Then run scar with `bin/scar start`.

Once running, scar exposes a number of HTTP resources:

 * `/graph.svg`
 * `/graph.dot`
 * `/graph.graphml`
 * `/flows.json`

Graphing begins as soon as Netflow templates have been
received. This may take several minutes, depending on the
configuration of your appliances.

## TODO

* Add `Statistics` GenServer to gather stats from GenStages
* Store and plot Scar statistics

